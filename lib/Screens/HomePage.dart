import 'package:flutter/material.dart';
import 'package:hidable/hidable.dart';

import 'Admin/login.dart';
import 'Admin/Admins/manageAdmin.dart';
import 'CategoriesPage.dart';
import 'WelcomePage.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentTabIndex = 0;
  ScrollController _scrollControler = ScrollController();
  @override
  Widget build(BuildContext context) {

    final TabPages = <Widget> [
      const WelcomePage(),
      CategoriesPage(controller: _scrollControler),
      UserManagementPage(),
      LoginPage()
    ];

    final BottomNavbarItems = <BottomNavigationBarItem>[
      const BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Acceuil'),
      const BottomNavigationBarItem(icon: Icon(Icons.category), label: 'Categories'),
      const BottomNavigationBarItem(icon: Icon(Icons.newspaper), label: 'News'),
            const BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Admin'),
    ];

    assert(TabPages.length == BottomNavbarItems.length);

    final bottomNavbar = BottomNavigationBar(
      items: BottomNavbarItems,
      currentIndex: _currentTabIndex,
      type: BottomNavigationBarType.fixed,
      onTap: (index){
        setState(() {
          _currentTabIndex = index;
        });
      },);

    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Bienvenue dans votre application Fassodegnouma') ,
      // ),
      body: TabPages[_currentTabIndex],
      bottomNavigationBar: Hidable(
        controller: _scrollControler,
        child: bottomNavbar
        ),
    );
  }
}