import 'package:flutter/material.dart';
import 'package:malifaso/Screens/LoisUer.dart';

import '../Widgets/SearchBar.dart';


import 'package:flutter/material.dart';

import '../Widgets/SearchBar.dart';
import 'Admin/InstitutionUser.dart';

class CategoriesPage extends StatefulWidget {
  CategoriesPage({required this.controller, super.key});
  ScrollController controller;
  @override
  State<CategoriesPage> createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {


  // Widget CategoriesCard(String imageUrl, String titre){
  //   return Container(
  //     margin: EdgeInsets.all(10),
  //     width: MediaQuery.of(context).size.width * 0.9,
  //     height: MediaQuery.of(context).size.width * 0.7,
  //     decoration: BoxDecoration(
  //       borderRadius: BorderRadius.circular(10),
  //       border: Border.all(width: 2)
  //     ),
  //     child: Card(
  //       child: Column(
  //         children: [
  //           Spacer(),
  //           Container(
  //             width: MediaQuery.of(context).size.width * 0.6,
  //             height: MediaQuery.of(context).size.width * 0.6,
  //             decoration: BoxDecoration(
  //               borderRadius: BorderRadius.circular(15)
  //             ),
  //             child: Image.asset(
  //               imageUrl,
  //               fit: BoxFit.cover,
  //             ),
  //           ),
  //           Spacer(),
  //           Text(
  //             titre,
  //             style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
  //           ),
  //           Spacer()
  //         ],
  //       ),
  //     ),
  //   );
  // }

  Widget CategoriesCard(String imageUrl, String titre, dynamic w){
  return GestureDetector(
    onTap: (){
      Navigator.pushNamed(context, w.id);
      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => LoisShow()));
    },
    child: Container(
      margin: const EdgeInsets.all(5),
      width: MediaQuery.of(context).size.width * 0.9,
      height: MediaQuery.of(context).size.width * 0.7,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(width: 2)
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Stack(
          children: [
            Positioned.fill(
              child: Ink.image(
                image: AssetImage(imageUrl),
                fit: BoxFit.cover,
                child: InkWell(
                  onTap: (){
                    //Navigator.pushNamed(context, w.id);
                    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => w));
                  }
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.transparent, Colors.black.withOpacity(0.7)]
                  )
                ),
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Text(
                  titre,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}



  @override
  Widget build(BuildContext context) {
    List<Widget> categories = [
      CategoriesCard("assets/images/amdeq-loi-travail-g.jpg", "Lois", const LoisShow()),
      CategoriesCard("assets/images/Institution.jpg", "Institution",const ShowInstitution()),
      CategoriesCard("assets/images/Histoire.jpg", "Histoire",const LoisShow()),
      CategoriesCard("assets/images/Geographies.jpg", "Geographie",const LoisShow())
    ];
    return Scaffold(
      appBar: AppBar(
        title: const Text("Categories"),
        leading: const Icon(Icons.category),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: 10),
            Row(
              children: [
                const Spacer(),
                const Icon(Icons.star, color: Colors.yellow,size: 40,),
                const Spacer(),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: const SearchSection()
                  ),
                  const Spacer()
              ],
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.6,
              child: ListView.builder(
                controller: widget.controller,
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                itemCount: 4,
                itemBuilder: (BuildContext context, int index) {
                  return categories[index];
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
