import 'package:flutter/material.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({super.key});

  static const String id = "welcome";

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Bienvenue dans Fassodegnouma') ,
      ),
      body: Column(
        children: [
          Row(children: [
            Spacer(),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.25,
              child: Image.asset("assets/images/flag.jpg")
              ),
              Spacer(),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.25,
              child: const Text("Republique du Mali")
              ),
              Spacer(),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.25,
              child: Image.asset("assets/images/embleme.jpg")
              ),
              Spacer()
          ],),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.9,
            child: Image.asset("assets/images/Hymme.jpg")
            )
        ],
      ),
    );
  }
}