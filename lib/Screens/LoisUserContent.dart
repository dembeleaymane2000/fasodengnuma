// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import '../Models/Lois.dart';

// class LoisContentUser extends StatefulWidget {
//   const LoisContentUser({required this.loi, super.key});

//   final Lois loi;

//   @override
//   State<LoisContentUser> createState() => _LoisContentUserState();
// }

// class _LoisContentUserState extends State<LoisContentUser> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text(widget.loi.code),),
//       body: SfPdfViewer.network(
//         widget.loi.files,
//         canShowScrollHead: false,
//         // customize the viewer as per your needs
//       ),
//     );
//   }
// }

class LoisContentUser extends StatefulWidget {
  final Lois loi;

  const LoisContentUser({Key? key, required this.loi}) : super(key: key);

  @override
  _LoisContentUserState createState() => _LoisContentUserState();
}

class _LoisContentUserState extends State<LoisContentUser> {
  bool _isLoading = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.loi.code,
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.share),
            onPressed: () {},
          ),
        ],
        backgroundColor: Colors.grey[800],
      ),
      body: Stack(
        children: [
          SfPdfViewer.network(
            widget.loi.files,
            canShowScrollHead: false,
            onPageChanged: (value) {
              setState(() {
                _isLoading = false;
              });
            },
            onDocumentLoadFailed: (PdfDocumentLoadFailedDetails details) {
              setState(() {
                _isLoading = false;
              });
            },
          ),
          if (_isLoading)
            const Center(
              child: CircularProgressIndicator(),
            ),
        ],
      ),
    );
  }
}
