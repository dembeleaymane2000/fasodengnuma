// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:malifaso/FirebaseManage.dart';
import 'package:malifaso/Models/Admin.dart';

class UserManagementPage extends StatefulWidget {
  @override
  _UserManagementPageState createState() => _UserManagementPageState();
}

class _UserManagementPageState extends State<UserManagementPage> {
  // ignore: prefer_final_fields
  FirebaseUtilities faso = FirebaseUtilities();
  Future<List<Admin>>? adminList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    adminList = faso.getAdmin();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: adminList,
        initialData: const [],
        builder: (_,snapshot){
        final list = snapshot.data;
        return ListView.builder(
        itemCount: list!.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            leading: const Icon(Icons.account_circle, size: 40,),
            title: Text(list[index].nom, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            subtitle: Text(list[index].email),
            trailing: IconButton(
              icon: const Icon(Icons.delete, color: Colors.red,size: 30,),
              onPressed: () {
                setState(() {
                  faso.deleteAdmin(list[index]);
                });
              },
            ),
            onTap: () {
              _showEditUserDialog(context, list[index]);
            },
          );
        },
      );
      }),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          _showAddAdminModalBottomSheet(context);
        },
      ),
    );
  }

  void _showEditUserDialog(BuildContext context, Admin adminstrator) {
    TextEditingController usernameController = TextEditingController(text: adminstrator.nom);
    TextEditingController emailController = TextEditingController(text: adminstrator.email);
    TextEditingController passwordController = TextEditingController(text: adminstrator.password);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Edit User'),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextFormField(
                  controller: usernameController,
                  decoration: const InputDecoration(
                    labelText: 'Username',
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter a username';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: emailController,
                  decoration: const InputDecoration(
                    labelText: 'Email',
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter an email';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: passwordController,
                  obscureText: true,
                  decoration: const InputDecoration(
                    labelText: 'Password',
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter a password';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 30),
                ElevatedButton(
                  onPressed: () {
                    if (usernameController.text.isNotEmpty &&
                        emailController.text.isNotEmpty &&
                        passwordController.text.isNotEmpty) {
                      setState(() {
                        adminstrator.nom = usernameController.text;
                        adminstrator.email = emailController.text;
                        adminstrator.password = passwordController.text;
                        faso.updateAdmin(adminstrator);
                        Navigator.pop(context);
                      });
                    }
                  },
                  child: const Text('Save Changes'),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
  void _showAddAdminModalBottomSheet(BuildContext context) {
    TextEditingController usernameController = TextEditingController();
    TextEditingController emailController = TextEditingController();
    TextEditingController passwordController = TextEditingController();

    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const Text(
                  'Add Admin',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: usernameController,
                  decoration: const InputDecoration(
                    labelText: 'Username',
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter a username';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: emailController,
                  decoration: const InputDecoration(
                    labelText: 'Email',
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter an email';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: passwordController,
                  obscureText: true,
                  decoration: const InputDecoration(
                    labelText: 'Password',
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter a password';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 30),
                ElevatedButton(
                  onPressed: () {
                    if (usernameController.text.isNotEmpty &&
                        emailController.text.isNotEmpty &&
                        passwordController.text.isNotEmpty) {
                      setState(() {
                        faso.addAdmin(Admin(
                          nom: usernameController.text,
                          email: emailController.text,
                          password: passwordController.text,
                          isActive: true,
                        ));
                        Navigator.pop(context);
                      });
                    }
                  },
                  child: const Text('Add Admin'),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
  }
