import 'package:flutter/material.dart';
import 'package:malifaso/FirebaseManage.dart';

import '../../Models/Institution.dart';

class InstitutionCard extends StatefulWidget {
  final Institution inst;

  const InstitutionCard({Key? key, required this.inst}) : super(key: key);

  @override
  _InstitutionCardState createState() => _InstitutionCardState();
}

class _InstitutionCardState extends State<InstitutionCard> {
  late bool isFavoris;
  FirebaseUtilities faso = FirebaseUtilities();
  @override
  void initState() {
    super.initState();
    isFavoris = widget.inst.isFavoris!;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      elevation: 5,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(
                widget.inst.image,
                width: 80,
                height: 80,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(width: 16),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.inst.description,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                  const SizedBox(height: 8),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        isFavoris = !isFavoris;
                        widget.inst.isFavoris = isFavoris;
                        faso.updateInstitutionFavoris(widget.inst);
                      });
                    },
                    child: Row(
                      children: [
                        Icon(
                          Icons.star,
                          color: isFavoris ? Colors.yellow : Colors.grey,
                          size: 20,
                        ),
                        const SizedBox(width: 4),
                        Text(
                          isFavoris ? "Retirer des favoris" : "Ajouter aux favoris",
                          style: TextStyle(
                            fontSize: 14,
                            color: isFavoris ? Colors.yellow : Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
