// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:malifaso/Screens/Admin/Lois/LoisAdmin.dart';
import 'package:malifaso/Screens/Admin/Lois/ManageLois.dart';

import '../../Models/Admin.dart';
import 'Institution.dart/AdminInstitution.dart';
import 'Admins/manageAdmin.dart';

class AdminWecomePage extends StatefulWidget {
  AdminWecomePage({required this.administrator, super.key});

  Admin administrator;

  @override
  State<AdminWecomePage> createState() => _AdminWecomePageState();
}

class _AdminWecomePageState extends State<AdminWecomePage> {

  List<Widget> list = [
    const AdminLois(),
    const AdminInstitution(),
    UserManagementPage(),
  ];
  int i = 0;
  Widget drawers(){
    return Drawer(
      child: Container(
        color: Colors.white,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: const BoxDecoration(
                color: Colors.blue,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  const CircleAvatar(
                    radius: 40,
                    backgroundColor: Colors.white,
                    child: Icon(
                      Icons.person,
                      size: 50,
                      color: Colors.blue,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    widget.administrator.nom,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 5),
                  Text(
                    widget.administrator.email,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            ),
            ListTile(
              leading: const Icon(Icons.home),
              title: const Text('Accueil'),
              onTap: () {
                // action lorsque l'utilisateur appuie sur la première option
              },
            ),
            ListTile(
              leading: const Icon(Icons.bookmark),
              title: const Text('Lois'),
              onTap: () {
                // action lorsque l'utilisateur appuie sur la deuxième option
                setState(() {
                  i = 0;
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              leading: const Icon(Icons.settings),
              title: const Text('Institutions'),
              onTap: () {
                // action lorsque l'utilisateur appuie sur la troisième option
                setState(() {
                  i = 1;
                  Navigator.pop(context);
                });
              },
            ),
            const Divider(),
            widget.administrator.email == "malifaso@gmail.com" &&  widget.administrator.password == "223"? ListTile(
              leading: const Icon(Icons.help),
              title: const Text('Manage Admin'),
              onTap: () {
                // action lorsque l'utilisateur appuie sur la quatrième option
                setState(() {
                  i = 2;
                  Navigator.pop(context);
                });
              },
            ): Container(),
            ListTile(
              leading: const Icon(Icons.info),
              title: const Text('À propos'),
              onTap: () {
                // action lorsque l'utilisateur appuie sur la cinquième option
              },
            ),
          ],
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: drawers(),
      appBar: AppBar(),
      body: list[i]
    );
  }
}