import 'package:flutter/material.dart';
import 'package:malifaso/Models/Institution.dart';

import '../../../FirebaseManage.dart';
import 'AddOrUpdate.dart';

class AdminInstitution extends StatefulWidget {
  const AdminInstitution({super.key});

  @override
  State<AdminInstitution> createState() => _AdminInstitutionState();
}

class _AdminInstitutionState extends State<AdminInstitution> {

  FirebaseUtilities faso = FirebaseUtilities();
  Future<List<Institution>>? institutionList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    institutionList = faso.getInstitution();
  }

  Widget institutionCard(Institution inst){
    return
    Card(
      child: ListTile(
        leading: Image.network(
          inst.image,
          width: 60,
          height: 60,
          fit: BoxFit.cover,
        ),
        title: Text(
          inst.nom,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            IconButton(
              icon: const Icon(
                Icons.edit,
                color: Colors.grey,
                size: 30,
              ),
              onPressed: (){
                setState(() {
                  Navigator.push(context,
                  MaterialPageRoute(builder: (context)=> AddUpdateInstitution(inst: inst)));
                });
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.delete,
                color: Colors.red,
                size: 30,
              ),
              onPressed: (){
                faso.deleteInstitution(inst);
                setState(() {
                  institutionList = faso.getInstitution();
                });
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: MaterialButton(
        child: const Icon(Icons.add),
        onPressed: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=> AddUpdateInstitution()));
      }),
    body: FutureBuilder(
      future: institutionList,
      initialData: const [],
      builder: (context, snapshot){
        final list = snapshot.data;
        return ListView.builder(
          itemCount: list!.length,
          itemBuilder: (_,i){
          return institutionCard(list[i]);
        });
    })
    );
  }
}