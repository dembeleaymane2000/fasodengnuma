import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:malifaso/FirebaseManage.dart';
import 'package:malifaso/Models/Institution.dart';

class AddUpdateInstitution extends StatefulWidget {
  AddUpdateInstitution({this.inst, Key? key}) : super(key: key);
  static const String id = "addupdate";
  Institution? inst;

  @override
  _AddUpdateInstitutionState createState() => _AddUpdateInstitutionState();
}

class _AddUpdateInstitutionState extends State<AddUpdateInstitution> {
  FilePickerResult? filess;
  final TextEditingController nomController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  FirebaseUtilities faso = FirebaseUtilities();

  @override
  Widget build(BuildContext context) {
    if(widget.inst!= null){
      nomController.text = widget.inst!.nom;
      descriptionController.text = widget.inst!.description;
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Add or update Institution",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.green,
      ),
      body: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey.shade400, width: 2.0),
          borderRadius: BorderRadius.circular(8.0),
        ),
        margin: const EdgeInsets.all(16.0),
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(height: 16.0),
            Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: nomController,
                    decoration: InputDecoration(
                      labelText: "Institution name",
                      labelStyle: const TextStyle(color: Colors.green),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 16.0),
                MaterialButton(
                  color: Colors.green,
                  textColor: Colors.white,
                  child: const Text("Add Image"),
                  onPressed: () async {
                    filess = await FilePicker.platform.pickFiles();
                  },
                ),
              ],
            ),
            const SizedBox(height: 16.0),
            Expanded(
              child: TextField(
                controller: descriptionController,
                decoration: InputDecoration(
                  labelText: "Description",
                  labelStyle: const TextStyle(color: Colors.green),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
                expands: true,
                maxLines: null,
                minLines: null,
              ),
            ),
            const SizedBox(height: 16.0),
            Row(
              children: [
                Expanded(
                  child: MaterialButton(
                    color: Colors.green,
                    textColor: Colors.white,
                    child: const Text("Add or Update"),
                    onPressed: () {
                      if(filess != null){
                      PlatformFile file = filess!.files.first;
                      if(widget.inst == null){
                        faso.addInstitution(
                          nomController.text.toString(),
                          descriptionController.text.toString(),
                          file);
                      }
                      else{
                        faso.updateInstitution(
                          nomController.text.toString(),
                          descriptionController.text.toString(),
                          widget.inst!.firebaseToken!,
                          file);
                      }
                    }
                    },
                  ),
                ),
                const SizedBox(width: 16.0),
                Expanded(
                  child: MaterialButton(
                    color: Colors.grey.shade300,
                    textColor: Colors.black,
                    child: const Text("Cancel"),
                    onPressed: () {
                      setState(() {
                        nomController.text = "";
                      descriptionController.text = "";
                      });
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
