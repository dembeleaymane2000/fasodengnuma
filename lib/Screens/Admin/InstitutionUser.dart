import 'package:flutter/material.dart';
import 'package:malifaso/Screens/Admin/InstitutionContent.dart';

import '../../FirebaseManage.dart';
import '../../Models/Institution.dart';
import 'institutionCard.dart';

class ShowInstitution extends StatefulWidget {
  const ShowInstitution({super.key});
  static const String id = "Institution";

  @override
  State<ShowInstitution> createState() => _ShowInstitutionState();
}

class _ShowInstitutionState extends State<ShowInstitution> {

  FirebaseUtilities faso = FirebaseUtilities();
  Future<List<Institution>>? institutionList;
  Color color= Colors.black;

//   Widget institutionCard(Institution inst){
//     return
//     Card(
//   shape: RoundedRectangleBorder(
//     borderRadius: BorderRadius.circular(10),
//   ),
//   margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
//   elevation: 5,
//   child: Container(
//     padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
//     child: Row(
//       crossAxisAlignment: CrossAxisAlignment.center,
//       children: [
//         ClipRRect(
//           borderRadius: BorderRadius.circular(10),
//           child: Image.network(
//             inst.image,
//             width: 80,
//             height: 80,
//             fit: BoxFit.cover,
//           ),
//         ),
//         const SizedBox(width: 16),
//         Expanded(
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Text(
//                 inst.description,
//                 style: const TextStyle(
//                   fontWeight: FontWeight.bold,
//                   fontSize: 18,
//                 ),
//               ),
//               const SizedBox(height: 8),
//               Row(
//                 children: [
//                   Icon(
//                     Icons.star,
//                     color: inst.isFavoris! ? Colors.yellow : Colors.grey,
//                     size: 20,
//                   ),
//                   const SizedBox(width: 4),
//                   Text(
//                     "Ajouter aux favoris",
//                     style: TextStyle(
//                       fontSize: 14,
//                       color: inst.isFavoris! ? Colors.yellow : Colors.grey,
//                     ),
//                   ),
//                 ],
//               ),
//             ],
//           ),
//         ),
//       ],
//     ),
//   ),
// );

//   }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    institutionList = faso.getInstitution();
  }

  @override
  Widget build(BuildContext context) {
    institutionList = faso.getInstitution();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Institution"),
      ),
      body: FutureBuilder(
      future: institutionList,
      initialData: const [],
      builder: (context, snapshot){
        final list = snapshot.data;
        return ListView.builder(
          itemCount: list!.length,
          itemBuilder: (_,i){
          return GestureDetector(
            onTap: () {
              Navigator.push(context,
              MaterialPageRoute(builder:
              (context)=> InstitutionContent(institution: list[i])
              )
              );
            },
            child: InstitutionCard(inst: list[i]),
          );
        });
    }),
    );
  }
}