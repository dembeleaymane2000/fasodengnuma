// ignore_for_file: library_private_types_in_public_api

import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:malifaso/FirebaseManage.dart';
import 'package:malifaso/Models/Lois.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class AdminLois extends StatefulWidget {
  const AdminLois({super.key});
  static const String id = "lois";

  @override
  _AdminLoisState createState() => _AdminLoisState();
}

class _AdminLoisState extends State<AdminLois> {
  // ignore: prefer_final_fields
  FirebaseUtilities faso = FirebaseUtilities();
  Future<List<Lois>>? loisList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loisList = faso.getLois();
  }

  @override
  Widget build(BuildContext context) {
    loisList = faso.getLois();
    return Scaffold(
      body: FutureBuilder(
        future: loisList,
        initialData: const [],
        builder: (_,snapshot){
        final list = snapshot.data;
        return Container(
          height: MediaQuery.of(context).size.height * 0.95,
          child: ListView.builder(
          itemCount: list!.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              leading: const Icon(Icons.account_circle, size: 40,),
              title: Text(list[index].code, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
              // subtitle: Text(list[index].email),
              trailing: SizedBox(
                width: MediaQuery.of(context).size.width * 0.3,
                child: Row(
                  children: [
                    IconButton(
                      icon: const Icon(Icons.edit, color: Colors.red,size: 30,),
                      onPressed: () {
                        setState(() {
                          _showUpdateAdminModalBottomSheet(context,list[index]);
                        });
                      },
                    ),
                    IconButton(
                      icon: const Icon(Icons.delete, color: Colors.red,size: 30,),
                      onPressed: () {
                        faso.deleteLois(list[index]);
                        setState(() {
                          loisList = faso.getLois();
                        });
                      },
                    ),
                  ],
                ),
              ),
              onTap: () {
                _showEditUserDialog(context, list[index]);
              },
            );
          },
              ),
        );
      }),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          _showAddAdminModalBottomSheet(context);
        },
      ),
    );
  }

  void _showEditUserDialog(BuildContext context, Lois adminstrator) {

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(adminstrator.code),
          content: SfPdfViewer.network(
        adminstrator.files,
        canShowScrollHead: false,
        // customize the viewer as per your needs
      ),
        );
      },
    );
  }
  void _showAddAdminModalBottomSheet(BuildContext context) {
    FilePickerResult? filess;
    TextEditingController usernameController = TextEditingController();

    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const Text(
                  'Add Lois',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: usernameController,
                  decoration: const InputDecoration(
                    labelText: 'Code',
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter law code';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 20),
                MaterialButton(
                  child:const Text("Choose File"),
                  onPressed: ()async {
                    filess = await FilePicker.platform.pickFiles();
                  }),
                const SizedBox(height: 30),
                ElevatedButton(
                  onPressed: () {
                    if(filess != null){
                      PlatformFile file = filess!.files.first;
                      faso.addLois(usernameController.text.toString(), file);
                      setState(() {
                        loisList = faso.getLois();
                      });
                    }
                  },
                  child: const Text('Add Lois'),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _showUpdateAdminModalBottomSheet(BuildContext context, Lois loi) {
    FilePickerResult? filess;
    TextEditingController usernameController = TextEditingController();

    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const Text(
                  'Update Lois',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: usernameController,
                  decoration: const InputDecoration(
                    labelText: 'Code',
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter law code';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 20),
                MaterialButton(
                  child:const Text("Choose File"),
                  onPressed: ()async {
                    filess = await FilePicker.platform.pickFiles();
                  }),
                const SizedBox(height: 30),
                ElevatedButton(
                  onPressed: () {
                    if(filess != null){
                      PlatformFile file = filess!.files.first;
                      faso.updateLois(loi, file);
                      setState(() {
                        loisList = faso.getLois();
                      });
                    }
                  },
                  child: const Text('update Lois'),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  }
