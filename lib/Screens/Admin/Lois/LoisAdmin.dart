// import 'package:flutter/material.dart';

// import '../../FirebaseManage.dart';
// import '../../Models/Lois.dart';

// class AdminLois extends StatefulWidget {
//   const AdminLois({super.key});

//   @override
//   State<AdminLois> createState() => _AdminLoisState();
// }

// class _AdminLoisState extends State<AdminLois> {
//   @override
//  Future<List<Lois>>? loi;
//   FirebaseUtilities faso = FirebaseUtilities();

//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     loi = faso.getLois();
//   }

//   Widget loisCard(String name, Function Function() f){
//     return ListTile(
//       leading: const Icon(Icons.label),
//       title: Text(name),
//       trailing: GestureDetector(
//         onTap: f,
//         child: const Icon(Icons.delete_outline),
//         ),
//     );
//   }
//   @override
//   Widget build(BuildContext context) {
//     return SingleChildScrollView(
//       child: FutureBuilder(
//         initialData: const [],
//         future: loi,
//         builder: ((context, snapshot) {
//           final list = snapshot.data;
//         return ListView.builder(
//           itemCount: list!.length,
//           itemBuilder: (BuildContext context, int index) {
//             return loisCard(list[index], (){
//               return (){faso.deleteLois(list[index]);};
//             });
//           }
//         );
//       })),
//     );
//   }
// }