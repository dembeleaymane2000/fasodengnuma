import 'package:flutter/material.dart';

import '../FirebaseManage.dart';
import '../Models/Lois.dart';
import 'LoisUserContent.dart';

class LoisShow extends StatefulWidget {
  const LoisShow({super.key});
  static const String id = "LoisShow";

  @override
  State<LoisShow> createState() => _LoisShowState();
}

class _LoisShowState extends State<LoisShow> {

  FirebaseUtilities faso = FirebaseUtilities();
  Future<List<Lois>>? loisList;

  bool isFavoris = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loisList = faso.getLois();
  }

  @override
  Widget build(BuildContext context) {
    loisList = faso.getLois();
    return Scaffold(
      appBar: AppBar(title:const Text("Liste lois")),
      body: FutureBuilder(
  future: loisList,
  initialData: const [],
  builder: (_, snapshot){
    final list = snapshot.data;
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.95,
      child: ListView.builder(
        itemCount: list!.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: ListTile(
              leading: Image.asset("assets/images/139.jpg"),
              title: Text(
                list[index].code,
                style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              subtitle: GestureDetector(
                onTap: () {
                  setState(() {
                    bool isFavoris = list[index].isFavoris;
                    isFavoris = !isFavoris;
                    list[index].isFavoris = isFavoris;
                    faso.updateLoisFavoris(list[index]);
                    loisList = faso.getLois();
                  });
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.star,
                      color: list[index].isFavoris ? Colors.yellow : Colors.grey,
                      size: 20,
                    ),
                    const SizedBox(width: 4),
                    Text(
                      list[index].isFavoris ? "Retirer des favoris" : "Ajouter aux favoris",
                      style: TextStyle(
                        fontSize: 14,
                        color: list[index].isFavoris ? Colors.yellow : Colors.grey,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoisContentUser(loi: list[index],)),
                );
              },
            ),
          );
        },
      ),
    );
  },
),

    );
  }
}