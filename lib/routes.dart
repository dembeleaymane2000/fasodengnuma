

import 'package:flutter/material.dart';
import 'package:malifaso/Screens/Admin/Institution.dart/AddOrUpdate.dart';

import 'Screens/LoisUer.dart';

final routes = {
  LoisShow.id: (BuildContext context) => const LoisShow(),
  AddUpdateInstitution.id: (BuildContext context) => AddUpdateInstitution(),
};
