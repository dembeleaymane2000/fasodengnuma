import 'package:cloud_firestore/cloud_firestore.dart';

class Institution{
  String nom;
  String description;
  String? firebaseToken;
  String image;
  bool? isFavoris;
  Institution(
    {required this.nom,
    required this.description,
    this.isFavoris,
    required this.image,
    this.firebaseToken});

    factory Institution.fromSnapshot(DocumentSnapshot<Map<String, dynamic>> file){
      final data = file.data();
      return Institution(
        firebaseToken: file.id,
        nom: data!["Nom"],
        isFavoris: data["Favoris"],
        description: data["Description"],
        image: data["Image"]);
    }
}