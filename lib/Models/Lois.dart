// ignore: file_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:malifaso/Models/Admin.dart';

class Lois{
  String code;
  String? firebaseToken;
  String files;
  Timestamp? addDate;
  bool isFavoris;
  Timestamp? modificationDate;
  Lois({required this.code,
   required this.files,
    this.firebaseToken,
     this.addDate,
     required this.isFavoris,
      this.modificationDate});

  factory Lois.fromSnapshot(DocumentSnapshot<Map<String,dynamic>> file){
    final data = file.data();
    return Lois(
      code: data!["Code"],
      files: data["File"],
      firebaseToken: file.id,
      addDate: data["Add Date"],
      modificationDate: data["Update Date"],
      isFavoris: data["Favoris"]
      );
  }

}