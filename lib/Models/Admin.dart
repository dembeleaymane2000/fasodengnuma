// ignore_for_file: file_names
import 'package:cloud_firestore/cloud_firestore.dart';

class Admin{
  String nom;
  String? firebaseToken;
  String email;
  String password;
  bool isActive;

  Admin({required this.nom, required this.email, required this.password, required this.isActive, this.firebaseToken});
  factory Admin.fromSnapshot(DocumentSnapshot<Map<String,dynamic>> file){
    final data = file.data();
    return Admin(nom: data!["Nom"],
    email: data["Email"],
      password: data["Password"],
      isActive: data["Active"],
      firebaseToken: file.id);
  }

}