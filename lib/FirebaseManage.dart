// ignore_for_file: file_names

import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:malifaso/Models/Admin.dart';
import 'package:malifaso/Models/Institution.dart';
import 'package:malifaso/Models/Lois.dart';

class FirebaseUtilities{
  final _db = FirebaseFirestore.instance;
  final _refs = FirebaseStorage.instance;

  Future<List<Admin>> getAdmin()async{
    final snapshot = await _db.collection("Admin").get();
    final userData = snapshot.docs.map((e) => Admin.fromSnapshot(e)).toList();
    return userData;
  }

  addAdmin(Admin administrator)async {
    await _db.collection("Admin").add({
      "Nom":administrator.nom,
      "Email":administrator.email,
      "Password":administrator.password,
      "Active":true
    });
  }

  updateAdmin(Admin administrator)async {
    await _db.collection("Admin").doc(administrator.firebaseToken).update({
      "Nom":administrator.nom,
      "Email":administrator.email,
      "Password":administrator.password,
      "Active":true
    });
  }

  deleteAdmin(Admin administrator)async {
    await _db.collection("Admin").doc(administrator.firebaseToken).delete();
  }

  Future<List<Lois>> getLois() async{
    final data = await _db.collection("Lois").get();
    return data.docs.map((e) => Lois.fromSnapshot(e)).toList();
  }

  addLois(String code, PlatformFile f)async{
    File file = File(f.path!);
    final store = _refs.ref().child("Fichier/${f.name}");
    await store.putFile(file);
    final downloaUrl = await store.getDownloadURL();
    await _db.collection("Lois").add({
      "Code": code,
      "File": downloaUrl,
      "Add Date": DateTime.now(),
      "Update Date": DateTime.now(),
      "Favoris":false
    });
  }

  updateLois(Lois loi, PlatformFile f)async{
    File file = File(f.path!);
    final store = _refs.ref().child("Fichier");
    await store.putFile(file);
    final downloaUrl = await store.getDownloadURL();
    await _db.collection("Lois").doc(loi.firebaseToken).update({
      "Code":loi.code,
      "File":downloaUrl,
      "Add Date":loi.addDate,
      "Update Date": loi.modificationDate,
      "Favoris":loi.isFavoris
    });
  }

  updateLoisFavoris(Lois loi) async{
    await _db.collection("Lois").doc(loi.firebaseToken).update({
      "Favoris":loi.isFavoris
    });
  }

  deleteLois(Lois loi) async{
    await _db.collection("Lois").doc(loi.firebaseToken).delete();
  }

Future<List<Institution>> getInstitution()async{
    final snapshot = await _db.collection("Institution").get();
    final userData = snapshot.docs.map((e) => Institution.fromSnapshot(e)).toList();
    return userData;
  }

  addInstitution(String nom,String desc , PlatformFile f)async {

    File file = File(f.path!);
    final store = _refs.ref().child("Fichier");
    await store.putFile(file);
    final downloaUrl = await store.getDownloadURL();
    await _db.collection("Institution").add({
      "Nom":nom,
      "Description":desc,
      "Image":downloaUrl,
      "Favoris":false
    });
  }

  updateInstitution(String nom,String desc , String token, PlatformFile f)async {
    File file = File(f.path!);
    final store = _refs.ref().child("Fichier");
    await store.putFile(file);
    final downloaUrl = await store.getDownloadURL();
    await _db.collection("Institution").doc(token).update({
      "Nom":nom,
      "Description":desc,
      "Image":downloaUrl,
      "Favoris":false
    });
  }

  updateInstitutionFavoris(Institution inst)async{
    await _db.collection("Institution").doc(inst.firebaseToken).update({
      "Favoris":inst.isFavoris
    });
  }


  deleteInstitution(Institution administrator)async {
    await _db.collection("Institution").doc(administrator.firebaseToken).delete();
  }


}